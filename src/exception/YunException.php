<?php

namespace yuntu\ThinkLibrary\exception;

use think\Exception;
use yuntu\ThinkLibrary\constant\DefaultConst;

/**
 * 云杉基础开发库异常抛出
 * @author weiss <1554783799@qq.com> 2022/6/20 15:15
 * @package yuntu\ThinkLibrary\exception
 */
class YunException extends Exception
{
    /**
     * @param string $message 通知语
     * @param int $code 状态码
     * @param array $data 回调参数
     */
    public function __construct(string $message = '', int $code = DefaultConst::HTTP_FAILED, array $data = [])
    {
        parent::setData(DefaultConst::EXCEPTION_YUN, $data);
        parent::__construct($message, $code);
    }
}
