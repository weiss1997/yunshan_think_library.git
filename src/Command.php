<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary;

/**
 * 自定义指令基类
 * @author weiss <1554783799@qq.com> 2022/4/19 15:26
 * @package app\common\command
 */
abstract class Command extends \think\console\Command
{
    /**
     * 进度起始选项
     */
    const PROGRESS_START = 'start';// 开始
    const PROGRESS_END = 'end';// 结束

    /**
     * 设置进度消息并继续执行
     * @param null|string $message 进度消息
     * @param null|string $progress 进度数值
     * @return $this
     */
    protected function setQueueProgress(?string $message = null, ?string $progress = null) : self
    {
        if (is_string($message)) {
            if ($progress == self::PROGRESS_START) {
                $message = "\033[0;32;40m" . $message;
            } else if ($progress == self::PROGRESS_END) {
                $message = "\033[0;32;40m$message\033[0m";
            }
            $this->output->writeln($message);
        }
        return $this;
    }

    /**
     * 更新任务进度
     * @param int $total 记录总和
     * @param int $count 当前记录
     * @param string $message 文字描述
     * @return $this
     */
    protected function setQueueMessage(int $total, int $count, string $message = '') : self
    {
        $total    = max($total, 1);
        $prefix   = str_pad("$count", strlen("$total"), '0', STR_PAD_LEFT);
        $progress = sprintf("%.2f", $count / $total * 100);
        return $this->setQueueProgress("\033[0m -\033[0;35;40m [$prefix/$total]\033[0m $message", $progress);
    }
}
