<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary;

use think\helper\Arr;
use yuntu\ThinkLibrary\db\query\Query;
use yuntu\ThinkLibrary\traits\ModelRegular;

/**
 * 基础模型类
 * @author weiss <1554783799@qq.com> 2022/3/29 18:09
 * @package yuntu\ThinkLibrary
 * @mixin Query
 */
abstract class Model extends \think\Model
{
    use ModelRegular;

    /**
     * 统一返回TextValue
     * @param mixed $value 状态值
     * @param array $array 状态组
     * @param array $except 排除key（已排除的key，全局业务状态查询时，不会再查询出来）
     * @return array|mixed|string
     */
    protected static function returnTextValue($value, array $array, array $except = [])
    {
        return $value === true ? Arr::except($array, $except) : ($array[$value] ?? '');
    }

    /**
     * @Todo 权限控制
     * @param array $data 实例化参数
     * @return Model
     */
    public static function AccessControl(array $data = []) : Model
    {
        return (new static($data))->field([])->visible([]);
    }
}
