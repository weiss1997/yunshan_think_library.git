<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

namespace yuntu\ThinkLibrary\traits;

/**
 * 验证器扩展规则
 * @author weiss <1554783799@qq.com> 2021/9/9 10:25
 * @package yuntu\ThinkLibrary\traits
 */
trait ValidateRule
{
    /**
     * 是否为JSON格式
     * @param string $value 字段值
     * @return bool
     */
    protected function isJson(string $value) : bool
    {
        if (is_string($value) && $content = htmlspecialchars_decode($value)) {
            $content = json_decode($content);
            if (($content && is_object($content)) || (is_array($content) && !empty($content))) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * 时间戳转换
     * @param string $date 时间
     * @param mixed $rule 验证规则
     * @param mixed $data 其它参数
     * @param string $field 字段名
     * @return bool
     */
    protected function strtotime(string $date, $rule, $data, string $field) : bool
    {
        $this->overrideData[$field] = strtotime($date);
        return true;
    }

    /**
     * 数组转字符串格式
     * @param array $array 数组
     * @param string $delimiter 分隔符
     * @param mixed $data 其它参数
     * @param string $field 字段名
     * @return bool
     */
    protected function arrayToString(array $array, string $delimiter, $data, string $field) : bool
    {
        $this->overrideData[$field] = implode($delimiter, $array);
        return true;
    }

    /**
     * 字符串转数组格式
     * @param array $string 字符串
     * @param string $delimiter 分隔符
     * @param mixed $data 其它参数
     * @param string $field 字段名
     * @return bool
     */
    protected function stringToArray(array $string, string $delimiter, $data, string $field) : bool
    {
        $this->overrideData[$field] = array_map('trim', explode($delimiter, $string));
        return true;
    }
}
