<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

namespace yuntu\ThinkLibrary\traits;

use think\exception\ValidateException;
use yuntu\ThinkLibrary\{Model, Validate};

/**
 * 常规模型方法扩展
 * @author weiss <1554783799@qq.com> 2022/5/11 11:22
 * @package yuntu\ThinkLibrary\traits
 */
trait ModelRegular
{
    /**
     * 常规数据创建
     * @param mixed|Validate $validate 验证器
     * @param string $scene 验证场景
     * @param array|null $post 提交参数
     * @return false|\think\Model|Model
     */
    public final static function _regularCreate($validate, string $scene, ?array $post = [])
    {
        $data  = $validate::aloneCheck($scene, $post);
        $model = static::create($data);
        if (!empty($model) && $model->id) {
            return $model;
        }
        return false;
    }

    /**
     * 常规数据修改
     * @param mixed|Validate $validate 验证器
     * @param string $scene 验证场景
     * @param array|null $post 提交参数
     * @return bool
     */
    public final static function _regularSave($validate, string $scene, ?array $post = []) : bool
    {
        $data = $validate::aloneCheck($scene, $post);
        $info = (new static)->where('id', $data['id'])->findOrFail();
        return $info->save($data);
    }

    /**
     * 常规状态更新
     * @param int|array $id 自增ID
     * @param int|string $status 状态值
     * @param string $field 字段名
     * @param array $saveVal 修改值
     * @return int
     */
    public final static function _regularStatusUpdate($id, $status, string $field = 'status', array $saveVal = [0, 1]) : int
    {
        if (!in_array($status, $saveVal)) throw new ValidateException('当前传入的状态值，不存在于可修改值内');

        $where[] = ['id', 'in', (array)$id];
        $where[] = [$field, '=', $status == $saveVal[0] ? $saveVal[1] : $saveVal[0]];
        return (int)(new static)->where($where)->update([$field => $status, 'update_time' => time()]);
    }

    /**
     * 常规数据删除
     * @param int|array $id 自增ID
     * @return int
     */
    public final static function _regularDelete($id) : int
    {
        return (int)(new static)->where('id', 'in', (array)$id)->delete();
    }
}
