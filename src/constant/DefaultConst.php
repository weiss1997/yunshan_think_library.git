<?php

namespace yuntu\ThinkLibrary\constant;

/**
 * 云杉基础开发库默认常量
 * @author weiss <1554783799@qq.com> 2022/6/20 15:12
 * @package yuntu\ThinkLibrary\constant
 */
class DefaultConst
{
    // +----------------------------------------------------------------------
    // | HTTP 响应
    // +----------------------------------------------------------------------

    const HTTP_SUCCESS = 200;// 请求成功
    const HTTP_ERROR = 400;// 请求失败
    const HTTP_FAILED = 0;// 响应错误
    const HTTP_NOT_FOUND = 404;// 404
    const HTTP_SERVER_ERROR = 500;// 服务器错误
    const HTTP_REDIRECT = 301;// URL重定向

    // +----------------------------------------------------------------------
    // | 命令行调用
    // +----------------------------------------------------------------------

    const COMMAND_CLEAR = 'yun:clear';// 清除缓存文件
    const COMMAND_DATABASE = 'yun:database';// 数据库修复优化
    const COMMAND_GLOBAL_MODEL_STATUS = 'yun:globalModelStatus';// 全局模型状态更新

    // +----------------------------------------------------------------------
    // | 异常抛出
    // +----------------------------------------------------------------------

    const EXCEPTION_YUN = 'YunException';// 云杉基础开发库异常抛出

    // +----------------------------------------------------------------------
    // | 系统管理服务
    // +----------------------------------------------------------------------

    // 运行环境
    const CONFIG_PARAMS_RUN_MODE = 'RunMode';// 运行模式
    const CONFIG_PARAMS_RUN_MODE_DEV = 'dev';// 测试服：本地开发 + 在线测试
    const CONFIG_PARAMS_RUN_MODE_ALPHA = 'alpha';// 内测服：内部测试版，仅内部人员使用
    const CONFIG_PARAMS_RUN_MODE_BETA = 'beta';// 公测服：公测演示版
    const CONFIG_PARAMS_RUN_MODE_STABLE = 'stable';// 正式服：稳定发布版

    // +----------------------------------------------------------------------
    // | 基础维护数据表
    // +----------------------------------------------------------------------

    const TABLES_CONFIG_PARAMS = 'config_params';// 【参数配置】应用参数配置表
}
