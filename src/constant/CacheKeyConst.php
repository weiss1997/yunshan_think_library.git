<?php

namespace yuntu\ThinkLibrary\constant;

/**
 * 缓存KEY常量
 * @author weiss <1554783799@qq.com> 2022/6/20 15:46
 * @package yuntu\ThinkLibrary\constant
 */
class CacheKeyConst
{
    const CACHE_TABLES = 'yun_cache_tables_';// 数据表名称缓存_不含前缀的数据表名字
    const CACHE_CONFIG_PARAMS = 'yun_cache_config_params';// 应用参数配置
    const CACHE_GLOBAL_MODEL_STATUS = 'yun_cache_global_model_status';// 全局模型状态
}
