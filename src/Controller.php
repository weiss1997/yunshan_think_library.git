<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary;

use think\{App, Paginator, Request, Response, Validate};
use think\exception\{HttpResponseException, ValidateException};
use yuntu\ThinkLibrary\constant\DefaultConst;

/**
 * 标准控制器基类
 * @author weiss <1554783799@qq.com> 2022/2/19 17:09
 * @package yuntu\ThinkLibrary
 */
abstract class Controller
{
    /**
     * Request 实例
     * @var Request
     */
    protected $request;

    /**
     * 应用实例
     * @var App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    /**
     * 云杉应用实例
     * @var Yun
     */
    protected $yun;

    /**
     * 构造方法
     * @access public
     * @param App $app 应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;
        $this->yun     = $app;

        // 控制器初始化
        $this->initialize();
    }

    /**
     * 初始化
     */
    protected function initialize()
    {
    }

    /**
     * 验证数据
     * @access protected
     * @param array $data 数据
     * @param string|array $validate 验证器名或者验证规则数组
     * @param array $message 提示信息
     * @param bool $batch 是否批量验证
     * @return bool
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false) : bool
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

    /**
     * 返回成功的操作
     * @param mixed $data 返回数据
     * @param string $msg 消息提示
     * @param array $header Header内容
     * @return Response
     */
    protected function success($data = [], string $msg = '操作成功', array $header = []) : Response
    {
        $result = [
            'code' => DefaultConst::HTTP_SUCCESS,
            'msg'  => $msg,
            'time' => $this->request->time(),
            'data' => $data,
        ];
        return Response::create($result, 'json')->header($header);
    }

    /**
     * 返回失败的操作
     * @param string $msg 消息提示
     * @param int $code 状态码
     * @param int $httpCode HTTP响应状态码
     * @return Response
     */
    protected function error(string $msg = '操作失败', int $code = DefaultConst::HTTP_FAILED, int $httpCode = 200) : Response
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'time' => $this->request->time(),
        ];
        return Response::create($result, 'json', $httpCode);
    }

    /**
     * URL重定向
     * @param string $url 跳转链接
     * @param integer $code 状态码
     */
    protected function redirect(string $url, int $code = DefaultConst::HTTP_REDIRECT) : void
    {
        throw new HttpResponseException(redirect($url, $code));
    }

    /**
     * 分页参数预设
     * @param int $page 分页页数
     * @param int $limit 每页条数
     * @return array
     */
    protected function page(int $page = 0, int $limit = 0) : array
    {
        return [
            'page'      => $page ?: $this->request->param('page/d', 1),
            'list_rows' => $limit ?: $this->request->param('limit/d', 10)
        ];
    }

    /**
     * 统一返回分页数据集
     * @param Paginator $list 分页数据
     * @param array $params 其它参数
     * @return Response
     */
    protected function paginate(Paginator $list, array $params = []) : Response
    {
        $return['count'] = $list->total();// 获取数据总条数
        $return['list']  = $list->items();// 获取数据列表
        return $this->success(array_merge($return, $params));
    }
}
