<?php

declare (strict_types=1);

namespace yuntu\ThinkLibrary\db\query;

use yuntu\ThinkLibrary\traits\WhereComplex;

/**
 * 指定查询对象
 * @author weiss <1554783799@qq.com> 2022/7/6 14:47
 * @package yuntu\ThinkLibrary\db\query
 */
class Query extends \think\db\Query
{
    use WhereComplex;
}
