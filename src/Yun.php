<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary;

use think\Service;
use app\admin\service\{AdminService};
use yuntu\ThinkLibrary\command\{Clear, Database, GlobalModelStatus};
use yuntu\ThinkLibrary\service\{SystemService, GlobalModelStatusService};

/**
 * 模块注册服务
 * @author weiss <1554783799@qq.com> 2022/3/30 10:08
 * @package yuntu\ThinkLibrary
 * @property SystemService $system 系统管理服务
 * @property GlobalModelStatusService $globalModelStatus 全局模型状态服务
 * @property AdminService $admin 管理员账号服务
 */
class Yun extends Service
{
    /**
     * 版本号
     */
    const VERSION = '1.0.0';

    /**
     * 绑定服务
     * @var array
     */
    public $bind = [];

    /**
     * @var Yun
     */
    public static $service;

    /**
     * 启动服务
     */
    public function boot()
    {
        // 注册 Think 指令
        $this->commands([
            Clear::class,
            Database::class,
            GlobalModelStatus::class
        ]);
    }

    /**
     * 初始化服务
     */
    public function register()
    {
        self::$service = $this->app->bind([
            'system'            => SystemService::class,
            'globalModelStatus' => GlobalModelStatusService::class,
            'admin'             => AdminService::class,
        ]);
    }
}
