<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary;

use think\exception\ValidateException;
use think\facade\Request;
use yuntu\ThinkLibrary\traits\ValidateRule;

/**
 * 标准验证器基类
 * @author weiss <1554783799@qq.com> 2022/3/19 9:53
 * @package yuntu\ThinkLibrary
 */
class Validate extends \think\Validate
{
    use ValidateRule;

    /**
     * 附加验证参数
     * @var array
     */
    protected $appendField = [];

    /**
     * 数据输出隐藏的属性
     * @var array
     */
    protected $hiddenField = [];

    /**
     * 定义字段空值转换null属性（适配数据库null默认值）
     * @var array
     */
    protected $nullField = [];

    /**
     * 自定义回调数据
     * @var array
     */
    protected $callData = [];

    /**
     * 支持参数覆盖、参数追加
     * @var array
     */
    protected $overrideData = [];

    /**
     * 设置验证数据与回调数据合并
     * @var bool
     */
    protected $isOverride = false;

    /**
     * 附加验证参数
     * @param string|array $data
     * @param string $value
     * @return $this
     */
    public function appendField($data = [], string $value = '') : self
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) $this->appendField[$key] = $value;
        } else if (is_string($data) && !empty($value)) {
            $this->appendField[$data] = $value;
        }
        return $this;
    }

    /**
     * 设置验证数据与回调数据合并
     * @return $this
     */
    public function isOverride() : self
    {
        $this->isOverride = true;
        return $this;
    }

    /**
     * 独立数据验证
     * @param string $scene 验证场景
     * @param null|array $post 请求参数
     * @param mixed $call 自定义回调数据
     * @return array
     */
    public static function aloneCheck(string $scene, ?array $post = [], &$call = []) : array
    {
        $mode = new static();
        $post = array_merge($post ?: Request::param(), $mode->appendField);
        if (empty($post)) {
            throw new ValidateException('空数据');
        }
        if (!$mode->scene($scene)->check($post)) {
            throw new ValidateException($mode->getError());
        }

        $call = $mode->callData;
        return $mode->getOnlyValidate($mode->isOverride ? array_merge($post, $call) : $post);
    }

    /**
     * 获取只读验证字段属性（只获取有值的字段）
     * @param array $field 参数字段
     * @return array
     */
    private function getOnlyValidate(array $field = []) : array
    {
        $data = [];
        foreach ($this->scene[$this->currentScene] ?? [] as $key) {
            if (!isset($field[$key])) continue;
            $val = $field[$key];
            if (
                (is_string($val) && strlen($val) > 0) ||
                (is_array($val) && !empty(array_filter($val))) ||
                (is_numeric($val) && !empty($val))
            ) {
                $data[$key] = $val;
            } else if (empty($val)) {
                $data[$key] = in_array($key, $this->nullField) ? null : '';
                foreach ($this->nullField as $nk => $nv) {
                    if (!is_int($nk) && $key == $nk) {
                        $data[$key] = $nv;
                        break;
                    }
                }
            }
        }

        foreach ($this->hiddenField as $val) {
            if (isset($data[$val])) unset($data[$val]);
        }

        return array_merge($data, $this->overrideData);
    }


    // @todo 待调整
    protected $setScene = false;
    protected $pk = 'id';// 数据表自增ID
    protected $isDel = 'is_del';// 数据删除标识字段

    /**
     * 验证数据是否要删除（当前行数据需要删除时，移除 "主键ID" 外的所有字段验证规则）
     * @param string $value 当前字段值
     * @param array $data 所有的数据
     * @return bool
     */
    protected function checkIsDel(string $value, array $data) : bool
    {
        if (isset($data['is_del']) && $data['is_del'] == 1) {
            $this->only(['id'])->callData['is_del'] = true;// 当前行数据允许被删除
            return false;
        }
        return true;
    }

    /**
     * 设置当前验证场景
     * @param string $scene 指定场景名称
     * @return $this
     */
    public function setScene(string $scene) : self
    {
        $this->setScene = $scene;
        return $this;
    }
}
