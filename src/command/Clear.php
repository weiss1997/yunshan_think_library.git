<?php

declare (strict_types=1);

namespace yuntu\ThinkLibrary\command;

use yuntu\ThinkLibrary\Command;
use yuntu\ThinkLibrary\constant\DefaultConst;
use think\console\{Input, Output};
use think\console\input\{Argument, Option};
use think\facade\App;

/**
 * 清除缓存文件
 * 基本命令与官网的一致，此清理指令是兼容多应用模式下的
 *
 * @author weiss <1554783799@qq.com> 2022/4/19 20:04
 * @package yuntu\ThinkLibrary\command
 *
 * 命令行调用示例：
 * @example 清除应用的缓存文件：php think yun:clear
 * @example 清理指定模块：php think yun:clear 模块名称
 * @example 清除日志目录：php think yun:clear --log
 * @example 清除日志目录并删除空目录：php think yun:clear --log --dir
 * @example 清除数据缓存目录：php think yun:clear --cache
 * @example 清除数据缓存目录并删除空目录：php think yun:clear --cache --dir
 * @example 如果需要清除某个指定目录下面的文件，可以使用：php think yun:clear --path d:\www\tp\runtime\log\
 */
class Clear extends Command
{
    /**
     * 指令任务配置
     */
    protected function configure()
    {
        $this->setName(DefaultConst::COMMAND_CLEAR)
            ->addOption('path', 'd', Option::VALUE_OPTIONAL, 'path to clear')
            ->addOption('cache', 'c', Option::VALUE_NONE, 'clear cache file')
            ->addOption('log', 'l', Option::VALUE_NONE, 'clear log file')
            ->addOption('dir', 'r', Option::VALUE_NONE, 'clear empty dir')
            ->addOption('expire', 'e', Option::VALUE_NONE, 'clear cache file if cache has expired')
            ->addOption('admin', 'f', Option::VALUE_NONE, '清理admin模块缓存')
            ->addArgument('dir', Argument::OPTIONAL, '应用目录名称')
            ->setDescription('清除缓存文件');
    }

    /**
     * 指令执行入口
     * @param Input $input
     * @param Output $output
     * @return void
     */
    protected function execute(Input $input, Output $output)
    {
        $runtimePath = App::getRootPath() . 'runtime' . DIRECTORY_SEPARATOR . '/' . ($input->getArgument('dir') ?: '') . '/';

        if ($input->getOption('cache')) {
            $path = $runtimePath . 'cache';
        } elseif ($input->getOption('log')) {
            $path = $runtimePath . 'log';
        } else {
            $path = $input->getOption('path') ?: $runtimePath;
        }

        $rmdir = (bool)$input->getOption('dir');
        // --expire 仅当 --cache 时生效
        $cache_expire = $input->getOption('expire') && $input->getOption('cache');
        $this->clear(rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR, $rmdir, $cache_expire);
        $output->write('清理成功');
    }

    /**
     * 清理文件
     * @param string $path 文件路径
     * @param bool $rmdir 是否删除目录
     * @param bool $cache_expire 缓存过期
     */
    protected function clear(string $path, bool $rmdir, bool $cache_expire) : void
    {
        $files = is_dir($path) ? scandir($path) : [];
        foreach ($files as $file) {
            if ('.' != $file && '..' != $file && is_dir($path . $file)) {
                $this->clear($path . $file . DIRECTORY_SEPARATOR, $rmdir, $cache_expire);
                if ($rmdir) {
                    @rmdir($path . $file);
                }
            } elseif ('.gitignore' != $file && is_file($path . $file)) {
                if ($cache_expire) {
                    if ($this->cacheHasExpired($path . $file)) {
                        unlink($path . $file);
                    }
                } else {
                    unlink($path . $file);
                }
            }
        }
    }

    /**
     * 缓存文件是否已过期
     * @param string $filename 文件路径
     * @return bool
     */
    protected function cacheHasExpired(string $filename) : bool
    {
        $content = file_get_contents($filename);
        $expire  = (int)substr($content, 8, 12);
        return 0 != $expire && time() - $expire > filemtime($filename);
    }
}
