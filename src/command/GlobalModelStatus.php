<?php

namespace yuntu\ThinkLibrary\command;

use yuntu\ThinkLibrary\constant\{DefaultConst, CacheKeyConst};
use yuntu\ThinkLibrary\Command;
use yuntu\ThinkLibrary\service\SystemService;
use think\console\{Input, Output};
use think\facade\{App, Cache};
use ReflectionClass, ReflectionException;

/**
 * 全局模型状态更新
 * @author weiss <1554783799@qq.com> 2022/4/6 1:41
 * @package yuntu\ThinkLibrary\command
 *
 * 命令行调用示例：
 * @example 全局模型状态更新：php think yun:globalModelStatus
 */
class GlobalModelStatus extends Command
{
    /**
     * 目录分隔符
     * @var string
     */
    private $DS = DIRECTORY_SEPARATOR;

    /**
     * 指的匹配的状态方法后缀
     * @var string
     */
    public $methodSuffix = 'TextAttr';

    /**
     * 指令任务配置
     */
    protected function configure()
    {
        $this->setName(DefaultConst::COMMAND_GLOBAL_MODEL_STATUS);
        $this->setDescription('全局模型状态更新');
    }

    /**
     * 指令执行入口
     * @param Input $input
     * @param Output $output
     * @return void
     * @throws ReflectionException
     */
    protected function execute(Input $input, Output $output)
    {
        $this->setQueueProgress('正在更新中......', self::PROGRESS_START);
        $list = $this->getMergeModels();
        Cache::set(CacheKeyConst::CACHE_GLOBAL_MODEL_STATUS, $list);
        $this->setQueueProgress('全局模型状态更新完成！', self::PROGRESS_END);
    }

    /**
     * 获取合并模型
     * @return array
     * @throws ReflectionException
     */
    public function getMergeModels() : array
    {
        $list = [];
        foreach (SystemService::instance()->getAppModule() as $val) {
            $moduleList = $this->getAllModuleGetter($val);
            foreach ($moduleList as $key => $mod) {
                $count = array_search($key, array_keys($moduleList));
                $this->setQueueMessage(count($moduleList), $count + 1, implode(' | ', $mod));
            }
            $list = array_merge($list, $moduleList);
        }
        return $list;
    }

    /**
     * 扫描指定应用模块下的模型获取器
     * @param string $dirName 应用目录名
     * @param string|null $multiFile 多级目录文件名
     * @return array
     * @throws ReflectionException
     */
    public function getAllModuleGetter(string $dirName, ?string $multiFile = '') : array
    {
        $retArr  = [];
        $dirFile = App::getBasePath() . $dirName . $this->DS . 'model' . $this->DS . ($multiFile ? $multiFile . $this->DS : '');
        if (is_dir($dirFile)) {
            $dh = opendir($dirFile);
            while (($file = readdir($dh)) !== false) {
                if ($file == '.' || $file == '..') continue;
                if (is_dir($dirFile . $file)) {
                    $retArr = array_merge($retArr, $this->getAllModuleGetter($dirName, $file));
                } else if (strstr($file, '.php')) {
                    $retArr = array_merge($retArr, $this->analysisModelGetter($dirFile . $file));
                }
            }
            closedir($dh);
        }
        return $retArr;
    }

    /**
     * 解析模型获取器方法
     * @param string $filePath 模型文件路径
     * @return array
     * @throws ReflectionException
     */
    public function analysisModelGetter(string $filePath) : array
    {
        $retArr     = [];
        $classPath  = str_replace('.php', '', substr($filePath, strripos($filePath, 'app' . $this->DS)));
        $reflection = new ReflectionClass($classPath);
        foreach ($reflection->getMethods() as $method) {
            if ($method->class == $classPath
                && $method->isPublic()
                && $method->isStatic()
                && is_callable([$classPath, $method->name])
                && preg_match_all('/(get)(.*)(?)(' . $this->methodSuffix . ')/', $method->name, $matches)
            ) {
                $modelName = substr($method->class, strripos($method->class, '\\') + 1);

                $retArr[$modelName . $matches[2][0]] = [$method->class, $method->name];
            }
        }
        return $retArr;
    }
}
