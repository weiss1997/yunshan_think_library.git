<?php

declare (strict_types=1);

namespace yuntu\ThinkLibrary\service;

use yuntu\ThinkLibrary\Service;
use yuntu\ThinkLibrary\exception\YunException;
use yuntu\ThinkLibrary\constant\{CacheKeyConst, DefaultConst};
use think\exception\ValidateException;
use think\db\exception\{DataNotFoundException, DbException, ModelNotFoundException};
use think\facade\{App, Cache, Db, Validate};
use think\Collection;

/**
 * 系统管理服务
 * @author weiss <1554783799@qq.com> 2022/4/19 20:36
 * @package yuntu\ThinkLibrary\service
 */
class SystemService extends Service
{
    /**
     * 获取所有应用模块
     * @return array
     */
    public function getAppModule() : array
    {
        $appName = [];
        $dirList = App::getAppPath();
        foreach (scandir($dirList) as $file) {
            if ($file == '.' || $file == '..') continue;
            if (is_dir($dirList . DIRECTORY_SEPARATOR . $file)) {
                $appName[] = $file;
            }
        }
        return $appName;
    }

    /**
     * 获取数据库所有数据表
     * @return array [table, total, count]
     */
    public function getTables() : array
    {
        $tables = $this->app->db->getTables();
        return [$tables, count($tables), 0];
    }

    /**
     * 判断是否存在指的数据表
     * @param string $tableName 不含前缀的数据表名字
     * @param bool $isCache 是否缓存检验
     * @return bool
     */
    public function existTables(string $tableName, bool $isCache = false) : bool
    {
        $cacheKey = CacheKeyConst::CACHE_TABLES . $tableName;
        if ($isCache && Cache::has($cacheKey)) {
            return (bool)Cache::get($cacheKey);
        }

        $tableName  = $this->app->config->get('database.connections.mysql.prefix') . $tableName;
        $existTable = !empty(Db::query("show tables like '$tableName'"));

        ($isCache && $existTable) && Cache::set($cacheKey, true);
        return $existTable;
    }

    /**
     * 验证数据表是否存在
     * @param string $table 数据表名称
     * @return bool
     */
    public function checkTable(string $table) : bool
    {
        $tables = $this->getTables()[0];
        return in_array($table, $tables);
    }

    /**
     * 规则合并
     * @param array $list 已扫描的规则列表
     * @param string $name 合并起点名称
     * @param string $delimiter 分隔符
     * @return array
     */
    public function rulesMerge(array $list, string $name, string $delimiter) : array
    {
        $combine = [];
        foreach ($list as $val) {
            $aiming = explode($delimiter, $val[$name]);
            $x      = [];
            $y      = &$x;
            while ($key = current($aiming)) {
                $y[$key] = $aiming[count($aiming) - 1] == $key ? $val : [];
                $y       = &$y[$key];
                next($aiming);
            }
            $combine = array_merge_recursive($combine, $x);
        }
        return $combine;
    }

    /**
     * 判断运行环境
     * @param string $type 运行模式（dev|alpha|beta|stable）
     * @return bool
     * @throws YunException
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function checkRunMode(string $type = DefaultConst::CONFIG_PARAMS_RUN_MODE_DEV) : bool
    {
        $host      = $this->app->request->host(true);
        $subDomain = $this->app->request->subDomain();
        foreach ($this->getParams(DefaultConst::CONFIG_PARAMS_RUN_MODE) as $key => $val) {
            if ($type == $key) {
                $val = $val ? explode(',', $val) : [];
                if (in_array($host, $val) || in_array($subDomain, $val)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取配置参数
     * @param string $tag 配置标签
     * @param string $key 获取指定标签参数
     * @return array|string
     * @throws YunException
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getParams(string $tag, string $key = '')
    {
        $tableName = DefaultConst::TABLES_CONFIG_PARAMS;
        if (!$this->existTables($tableName, true)) {
            throw new YunException('数据表：' . $tableName . ' 不存在，请先维护此表！');
        }

        $paramsList = [];
        $configList = Db::name($tableName)
            ->cache(CacheKeyConst::CACHE_CONFIG_PARAMS)
            ->field('config_tag,config_key,config_value')
            ->select();
        foreach ($configList as $val) {
            $paramsList[$val['config_tag']][$val['config_key']] = $val['config_value'];
        }
        return $key ? ($paramsList[$tag][$key] ?? '') : $paramsList[$tag] ?? [];
    }

    /**
     * 设置配置参数
     * @param string $tag 配置标签
     * @param string $key 配置索引
     * @param mixed $value 参数内容
     * @return mixed
     * @throws DbException
     */
    public static function setParams(string $tag, string $key, $value)
    {
        $where[] = ['config_tag', '=', $tag];
        $where[] = ['config_key', '=', $key];
        Db::name(DefaultConst::TABLES_CONFIG_PARAMS)
            ->cache(CacheKeyConst::CACHE_CONFIG_PARAMS)
            ->where($where)
            ->update(['config_value' => $value]);
        return $value ?: '';
    }

    /**
     * 更新配置参数
     * @param mixed $model 应用配置参数模型 ConfigParams
     * @param Validate $validate 验证器
     * @param null|array $data 更新数据
     * @return Collection
     */
    public static function updateParams($model, Validate $validate, ?array $data) : Collection
    {
        if (empty($data)) throw new ValidateException('没有可更新的配置参数！');
        $validate->rule('id', 'require');
        foreach ($data as $val) {
            if (!$validate->check($val)) throw new ValidateException($validate->getError());
        }
        Cache::delete(CacheKeyConst::CACHE_CONFIG_PARAMS);
        return (new $model)->saveAll($data);
    }

    /**
     * 复制并创建表结构
     * @param string $from 来源表名
     * @param string $create 创建表名
     * @param array $tables 现有表集合
     * @param bool $copy 是否复制
     * @param mixed $where 复制条件
     * @throws YunException
     */
    public function copyTableStruct(string $from, string $create, array $tables = [], bool $copy = false, $where = []) : void
    {
        if (empty($tables)) [$tables] = $this->getTables();
        if (!in_array($from, $tables)) {
            throw new YunException("待复制的数据表 $from 不存在！");
        }
        if (!in_array($create, $tables)) {
            $this->app->db->query("CREATE TABLE IF NOT EXISTS $create (LIKE $from)");
            if ($copy) {
                $sql1 = $this->app->db->name($from)->where($where)->buildSql(false);
                $this->app->db->query("INSERT INTO $create $sql1");
            }
        }
    }

    /**
     * 清除缓存文件
     */
    public function clearRuntime() : void
    {
        $this->app->cache->clear();
        $this->app->console->call('clear', ['--dir']);
    }
}
