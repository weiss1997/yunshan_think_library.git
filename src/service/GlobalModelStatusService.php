<?php

namespace yuntu\ThinkLibrary\service;

use think\facade\{Cache, console};
use think\{Container, Exception};
use think\exception\ClassNotFoundException;
use yuntu\ThinkLibrary\constant\{CacheKeyConst, DefaultConst};
use yuntu\ThinkLibrary\exception\YunException;

/**
 * 全局模型状态服务
 * @author weiss <1554783799@qq.com> 2022/6/20 17:28
 * @package yuntu\ThinkLibrary\service
 */
class GlobalModelStatusService
{
    /**
     * 查询状态
     * @param string|array $name 模型名
     * @return array
     * @throws YunException
     */
    public function queryStatus($name = [])
    {
        if (!empty($name) && is_string($name)) {
            $name = explode(',', $name);
        }

        try {
            $data = [];
            $list = $this->getModelList();
            foreach ($name as $val) {
                if ($model = $list[$val] ?? null) {
                    $data[$val] = Container::getInstance()->make($model[0])->{$model[1]}(true);
                }
            }
            return $data;
        } catch (ClassNotFoundException $e) {
            throw new YunException('状态模型异常！');
        } catch (Exception $e) {
            throw new YunException('状态查询异常！');
        }
    }

    /**
     * 获取模型列表
     * @return mixed
     */
    public function getModelList()
    {
        $cacheKey = CacheKeyConst::CACHE_GLOBAL_MODEL_STATUS;
        if (!Cache::has($cacheKey)) {
            $this->updateModelList();
        }
        return Cache::get($cacheKey);
    }

    /**
     * 更新模型列表
     * @return string
     */
    public function updateModelList() : string
    {
        $output = Console::call(DefaultConst::COMMAND_GLOBAL_MODEL_STATUS);
        return $output->fetch();
    }
}
